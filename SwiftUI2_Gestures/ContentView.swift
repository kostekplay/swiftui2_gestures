////  ContentView.swift
//  SwiftUI2_Gestures
//
//  Created on 09/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    
    @GestureState var isLongPressed = false
    
    @State private var offset: CGSize = .zero
    
    var body: some View {
        
        let longPressGesture = LongPressGesture()
            .updating($isLongPressed) { (value, state, transaction) in
                state = value
            }
        
        let dragGesture = DragGesture()
            .onChanged { (value) in
                let point = value.location
                self.offset = value.translation
            }
        
        VStack {
            Circle()
                .frame(width: 150, height: 150, alignment: .center)


                //isLongPressed
//                .foregroundColor(isLongPressed ? Color.gray : Color.blue)
//                .scaleEffect(isLongPressed ? 2 : 1)
//                .gesture(longPressGesture)
//                .animation(.spring())
            
                // dragGesture
                .offset(x: offset.width, y: offset.height)
                .gesture(dragGesture)
                .animation(.default)
            
        }
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
